﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StackImplementation
{
    public class ExceededSizeException : Exception
    {      
        public ExceededSizeException(int index)
        : base(String.Format($"Stack Overflow index position {index} is higher than upper limit of stack"))
        {

        }
    }
}
