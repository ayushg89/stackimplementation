﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StackImplementation
{
    public class ExpenditureProhibitedException:Exception
    {
        public ExpenditureProhibitedException(int index)
        : base(String.Format($"Stack underflow : No element at index position {index}"))
        {

        }
    }
}
