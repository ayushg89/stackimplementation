﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StackImplementation
{
    public class StackDS<T> where T : struct
    {
        private T[] stackArray;
        private int top;
        private int _maximumLength;
        public int Size { get; private set; }             
        public StackDS(int maximumLength)
        {
            _maximumLength = maximumLength;
            stackArray = new T[_maximumLength];
            top = -1;
        }
        public int GetCurrentSize()
        {
            return top + 1;
        }
        public void Push(T value)
        {
            if (top >= _maximumLength - 1)
            {
                throw new ExceededSizeException(top);
            }
            else
            {
                stackArray[++top] = value;
            }           
        }
        public T Pop()
        {
            T value;         
            if (top < 0)
            {
                throw new ExpenditureProhibitedException(top);
            }
            else
            {
                value = stackArray[top--];
                return value;
            }           
        }
        public T Peek()
        {           
            if (top < 0)
            {
                throw new ExpenditureProhibitedException(top);
            }
            else
            {
                return stackArray[top];
            }           
        }       
        public void PrintStack()
        {
            if (top < 0)
            {
                Console.WriteLine("Stack is empty");
                return;
            }
            else
            {
                Console.WriteLine("Items in the Stack are :");
                for (int i = top; i >= 0; i--)
                {
                    Console.WriteLine(stackArray[i]);
                }
            }
        }
    }
}
