﻿using System;

namespace StackImplementation
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter maximum size of Stack");
            int maxSize = Convert.ToInt32(Console.ReadLine());
            StackDS<int> myStack = new StackDS<int>(maxSize);
            while (true)
            {
                Console.Clear();
                Console.WriteLine("\nStack MENU");
                Console.WriteLine("1. Add an element");
                Console.WriteLine("2. See the Top element.");
                Console.WriteLine("3. Remove top element.");
                Console.WriteLine("4. Display current size of stack.");
                Console.WriteLine("5. Display stack elements.");
                Console.WriteLine("6. Exit");
                Console.WriteLine("Select your choice: ");
                int choice = Convert.ToInt32(Console.ReadLine());
                switch (choice)
                {
                    case 1:
                        Console.WriteLine("Enter an Element : ");
                        myStack.Push(Convert.ToInt32(Console.ReadLine()));
                        break;

                    case 2:
                        Console.WriteLine("Top element is: {0}", myStack.Peek());
                        break;

                    case 3:
                        Console.WriteLine("Element removed: {0}", myStack.Pop());
                        break;
                    case 4:
                        Console.WriteLine("Current Size of stack is: {0}", myStack.GetCurrentSize());
                        break;
                    case 5:
                        myStack.PrintStack();
                        break;
                    case 6:
                        System.Environment.Exit(1);
                        break;
                }               
                Console.ReadKey();
            }            
        }
    }
}
