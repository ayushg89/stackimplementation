using NUnit.Framework;
using System;

namespace StackImplementation.Test
{
    public class StackTest
    {
        private StackDS<int> myStack;
        [SetUp]
        public void Setup()
        {
            myStack = new StackDS<int>(3);
        }

        [Test]
        public void Creation()
        {
            Assert.AreEqual(0, myStack.GetCurrentSize());
        }
        [Test]
        public void Push_Pop()
        {
            myStack.Push(3);
            myStack.Push(5);              
            Assert.AreEqual(2, myStack.GetCurrentSize());
            int value=myStack.Pop();
            Assert.AreEqual(5, value);
        }

        [Test]

        public void Too_Much_Pop()
        {
            myStack.Push(3);
            myStack.Pop();
            Assert.Throws<ExpenditureProhibitedException>(() => myStack.Pop());
        }

        [Test]
        public void Too_Much_Push()
        {
            myStack.Push(1);
            myStack.Push(2);
            myStack.Push(3);
            Assert.Throws<ExceededSizeException>(() => myStack.Push(4));
        }

        [Test]
        public void Peek_Exception()
        {
            Assert.Throws<ExpenditureProhibitedException>(() => myStack.Peek());
        }

        [Test]
        public void Peek_Element()
        {
            myStack.Push(1);
            myStack.Push(2);
            int value = myStack.Peek();
            Assert.AreEqual(2, value);
            Assert.AreEqual(2, myStack.GetCurrentSize());
        }

    }
}